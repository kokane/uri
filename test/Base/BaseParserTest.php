<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace KokaneTest\Uri\Parser;

use Kokane\Uri\Base\BaseHandler;
use Kokane\Uri\Base\BaseParser;
use Kokane\Uri\Parser\ParserInterface;
use Kokane\Uri\Parser\Standard as UriParser;
use Kokane\Uri\Uri;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class BaseParserTest extends TestCase
{
    /**
     * Data providder.
     * 
     * @return array
     */
    public static function getBaseHandlerData()
    {
        return [
            array(
                'http://example.com/projects/name/index.php?title=test',
                '/projects/name',
                '/index.php',
            ),
            array(
                'http://example.com/base/of/project/folder',
                '/base/of/project',
                '/folder',
            ),
            array(
                'http://example.com/a/b/c/d/e/f',
                '/a/b/c/d/e',
                '/f',
            ),
        ];
    }

    /**
     * @dataProvider getBaseHandlerData
     */
    public function testGetPrefix($uri, $prefix, $path)
    {
        $base = $this->getBase($uri, $prefix);

        $this->assertEquals($base, $path);
        $this->assertEquals($prefix, $base->getPrefix());
    }

    /**
     * @dataProvider getBaseHandlerData
     */
    public function testGetPath($uri, $prefix, $path)
    {
        $base = $this->getBase($uri, $prefix);

        $this->assertEquals($base, $path);
        $this->assertEquals($path, $base->getPath());
    }

    public function testWithNoMatchPrefixReturnNull()
    {
        $uri = 'http://example.com/base/of/project/folder';
        $prefix = 'base/of/project';

        $base = $this->getBase($uri, $prefix);

        $this->assertEquals($prefix, $base->getPrefix());
        $this->assertNull($base->getPath());
    }

    public function testCreateInstanceOfBaseWithoutPrefix()
    {
        $base = new \Kokane\Uri\Base\Base('/prefix');

        $this->assertEquals('/prefix', $base->getPrefix());
    }

    /**
     * Returns an instance of BaseHandler.
     * 
     * @param  string $content
     * @param  string $prefix
     * @return BaseHandler
     */
    private function getBase($content, $prefix)
    {
        $uri = $this->getUriInstance($content, $prefix);
        $parser = $this->getUriParserInstance();
        $parser = $this->getBaseParserInstance($parser);

        $parser->parse($uri);

        return $uri->getBase();
    }

    /**
     * Returns an instance of uri.
     * 
     * @param  string $uri
     * @param  string $prefix
     * @return Uri
     */
    private function getUriInstance($uri, $prefix)
    {
        return new Uri($uri, $prefix);
    }

    /**
     * Returns an insntance of uri parser.
     * 
     * @return UriParser
     */
    private function getUriParserInstance()
    {
        return new UriParser();
    }

    /**
     * Returns an instance of base parser.
     * 
     * @param  ParserInterface $parser
     * @return BaseParser
     */
    public function getBaseParserInstance(ParserInterface $parser)
    {
        return new BaseParser($parser);
    }
}
