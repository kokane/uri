<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Query;

use ArrayObject;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class Query extends ArrayObject implements QueryHandler
{
    /**
     * @var string 
     */
    private $rawString;

    /**
     * {@inheritdoc}
     */
    public function set($key, $param)
    {
        $this->offsetSet($key, $param);
    }

    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
        return $this->has($key) ? $this->offsetGet($key) : $default;
    }

    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        return $this->offsetExists($key);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key)
    {
        $this->offsetUnset($key);
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        $this->exchangeArray([]);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function extend(array $params)
    {
        $this->exchangeArray(array_merge($this->getArrayCopy(), $params));

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
        return $this->count() == 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getKeys()
    {
        return array_keys($this->getArrayCopy());
    }

    /**
     * {@inheritdoc}
     */
    public function setParams(array $params)
    {
        $this->exchangeArray($params);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParams()
    {
        return $this->getArrayCopy();
    }

    /**
     * {@inheritdoc}
     */
    public function setRawString($rawString)
    {
        $this->rawString = (string) $rawString;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRawString()
    {
        return $this->rawString;
    }

    /**
     * {@inheritdoc}
     */
    public function getValues()
    {
        return array_values($this->getArrayCopy());
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return http_build_query($this->getArrayCopy());
    }
}
