<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri;

use Kokane\Uri\Base\Base;
use Kokane\Uri\Base\BaseHandler;
use Kokane\Uri\Exception\UnexpectedTypeException;
use Kokane\Uri\Query\Query;
use Kokane\Uri\Query\QueryHandler;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class Uri
{
    const SCHEME_HTTP = 'http';
    const SCHEME_HTTPS = 'https';

    /**
     * @var string 
     */
    private $uri;

    /**
     * @var string 
     */
    private $scheme = self::SCHEME_HTTP;

    /**
     * @var string 
     */
    private $host;

    /**
     * @var integer
     */
    private $port = 80;

    /**
     * @var string 
     */
    private $user;

    /**
     * @var string 
     */
    private $pass;

    /**
     * @var string 
     */
    private $path;

    /**
     * @var string 
     */
    private $fragment;

    /**
     * @var BaseHandler 
     */
    private $base = null;

    /**
     * @var QueryHandler 
     */
    private $query = null;

    /**
     * Constructor.
     * 
     * @param  string $uri
     * @param  string $prefix
     * @param  boolean decoded
     * @return void
     */
    public function __construct($uri = null, $prefix = null, $decoded = true)
    {
        if (null !== $uri) {
            $this->setUri($uri, $decoded);
        }

        if (null !== $prefix) {
            $this->getBase()->setPrefix($prefix);
        }
    }

    /**
     * Returns the Uri.
     * 
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Returns the scheme.
     * 
     * @return string
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * Returns the host.
     * 
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Returns the port.
     * 
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Returns the username.
     * 
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Returns the password.
     * 
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Returns the path.
     * 
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Returns the fragment.
     * 
     * @return string
     */
    public function getFragment()
    {
        return $this->fragment;
    }

    /**
     * Returns the base handler.
     * 
     * @return BaseHandler
     */
    public function getBase()
    {
        if (null === $this->base) {
            $this->base = new Base();
        }

        return $this->base;
    }

    /**
     * Returns the query handler.
     * 
     * @return QueryHandler
     */
    public function getQuery()
    {
        if (null === $this->query) {
            $this->query = new Query();
        }

        return $this->query;
    }

    /**
     * Sets the uri content string.
     * 
     * @param  string $uri
     * @param  bool $decoded
     * @return Uri
     * @throws UnexpectedTypeException
     */
    public function setUri($uri, $decoded = true)
    {
        if (is_string($uri) === false) {
            throw new UnexpectedTypeException("$uri must be a string");
        }

        $this->uri = $uri;

        if (boolval($decoded)) {
            $this->decode();
        }

        return $this;
    }

    /**
     * Sets the scheme.
     * 
     * @param  string $scheme
     * @return Uri
     */
    public function setScheme($scheme)
    {
        $this->scheme = (string) $scheme;

        return $this;
    }

    /**
     * Sets the host.
     * 
     * @param  string $host
     * @return Uri
     */
    public function setHost($host)
    {
        $this->host = (string) $host;

        return $this;
    }

    /**
     * Sets the port.
     * 
     * @param  string $port
     * @return Uri
     */
    public function setPort($port)
    {
        $this->port = (int) $port;

        return $this;
    }

    /**
     * Sets the user.
     * 
     * @param  string $user
     * @return Uri
     */
    public function setUser($user)
    {
        $this->user = (string) $user;

        return $this;
    }

    /**
     * Sets the password.
     * 
     * @param  string $pass
     * @return Uri
     */
    public function setPass($pass)
    {
        $this->pass = (string) $pass;

        return $this;
    }

    /**
     * Sets the base handler.
     * 
     * @param  BaseHandler $base
     * @return Uri
     */
    public function setBase(BaseHandler $base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Sets the path.
     * 
     * @param  string $path
     * @return Uri
     */
    public function setPath($path)
    {
        $this->path = (string) $path;

        return $this;
    }

    /**
     * Sets the query handler.
     * 
     * @param  QueryHandler $query
     * @return Uri
     */
    public function setQuery(QueryHandler $query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Sets the fragment.
     * 
     * @param  string $fragment
     * @return Uri
     */
    public function setFragment($fragment)
    {
        $this->fragment = (string) $fragment;

        return $this;
    }

    /**
     * Whether the https scheme is on.
     * 
     * @return bool
     */
    public function isSecure()
    {
        return self::SCHEME_HTTPS === $this->getScheme();
    }

    /**
     * Encodes the uri.
     * 
     * @return string
     */
    public function encode()
    {
        $this->uri = urlencode($this->uri);

        return $this;
    }

    /**
     * Decodes the uri.
     * 
     * @return string
     */
    public function decode()
    {
        $this->uri = urldecode($this->uri);

        return $this;
    }

    /**
     * Returns uri value's string representation.
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->getUri();
    }
}
