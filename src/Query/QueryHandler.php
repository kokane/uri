<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Query;

use ArrayAccess;
use Countable;
use IteratorAggregate;
use Serializable;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
interface QueryHandler extends
    IteratorAggregate,
    ArrayAccess,
    Serializable,
    Countable
{
    /**
     * Sets the parameter.
     * 
     * @return void
     */
    public function set($key, $param);

    /**
     * Returns a parameter by name.
     * 
     * @param  mixed $key
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * Returns true if the parameter is defined.
     * 
     * @param  string $key
     * @return boolean
     */
    public function has($key);

    /**
     * Removes a parameter.
     * 
     * @param  mixed $key
     * @return void
     */
    public function remove($key);

    /**
     * Clears all params.
     * 
     * @return QueryHandler
     */
    public function clear();

    /**
     * Extends the params.
     * 
     * @param  array $params
     * @return QueryHandler
     */
    public function extend(array $params);

    /**
     * Returns true if this query contains no key-value mappings.
     * 
     * @return boolean
     */
    public function isEmpty();

    /**
     * Returns the parameter keys.
     * 
     * @return array
     */
    public function getKeys();

    /**
     * Sets the params.
     * 
     * @param  array $params
     * @return QueryHandler
     */
    public function setParams(array $params);

    /**
     * Returns all parameters.
     * 
     * @return array
     */
    public function getParams();

    /**
     * Sets the raw string.
     * 
     * @param  string $rawString
     * @return QueryHandler
     */
    public function setRawString($rawString);
 
    /**
     * Returns the raw string.
     * 
     * @return QueryHandler
     */
    public function getRawString();

    /**
     * Returns the values of the parameters.
     * 
     * @return array
     */
    public function getValues();

    /**
     * Returns query value's string representation.
     * 
     * @return string
     */
    public function __toString();
}
