# Kokane URI

***

## Introduction

Kokane URI is a component that aids in manipulating Uniform Resource Identifier.

## Installation

Install libraries
```
composer require "kokane/uri"
```
or
```
composer require "kokane/uri":"dev-master"
```

## Usage

```php
<?php

require __DIR__ . '/vendor/autoload.php';

use Kokane\Uri\Base\BaseParser;
use Kokane\Uri\Parser\Standard;
use Kokane\Uri\Query\QueryParser;
use Kokane\Uri\Uri;

$content = 'https://php.net/projects/folder/index.php?langage=PHP#signet';

$uri = new Uri($content, '/projects/folder');

$parser = new Standard();
$parser = new BaseParser($parser);
$parser = new QueryParser($parser);

$parser->parse($uri);

if ($uri->isSecure()) {
    printf('%s : %s', $uri->getHost(), $uri->getBase()); // php.net : /index.php
}
```

## License

Kokane Uri is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)