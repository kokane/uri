<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Parser;

use Kokane\Uri\Uri;

/**
 * Standard is an Uri parser.
 * 
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class Standard implements ParserInterface
{
    const ATTR_SCHEME = 'scheme';
    const ATTR_HOST = 'host';
    const ATTR_PORT = 'port';
    const ATTR_USER = 'user';
    const ATTR_PASS = 'pass';
    const ATTR_PATH = 'path';
    const ATTR_QUERY = 'query';
    const ATTR_FRAGMENT = 'fragment';

    /**
     * @var array 
     */
    private $defaultParts = array(
        self::ATTR_SCHEME => 'http',
        self::ATTR_HOST => '',
        self::ATTR_PORT => 80,
        self::ATTR_USER => '',
        self::ATTR_PASS => '',
        self::ATTR_PATH => '',
        self::ATTR_QUERY => '',
        self::ATTR_FRAGMENT => '',
    );

    /**
     * @var array 
     */
    private $parts = [];

    /**
     * {@inheritdoc}
     * 
     * @see http://tools.ietf.org/html/rfc3986#appendix-B
     */
    public function parse(Uri $uri)
    {
        $this->setParts(parse_url($uri->getUri()));

        $uri->setScheme($this->getPart(self::ATTR_SCHEME))
            ->setUser($this->getPart(self::ATTR_USER))
            ->setPass($this->getPart(self::ATTR_PASS))
            ->setHost($this->getPart(self::ATTR_HOST))
            ->setPort($this->getPart(self::ATTR_PORT))
            ->setPath($this->getPart(self::ATTR_PATH))
            ->setFragment($this->getPart(self::ATTR_FRAGMENT))
            ->getQuery()->setRawString($this->getPart(self::ATTR_QUERY));
    }

    /**
     * Returns all parts.
     * 
     * @return array
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * Returns given value's part, if exists.
     * 
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function getPart($key, $default = null)
    {
        return array_key_exists($key, $this->parts) ? $this->parts[$key] : $default;
    }

    /**
     * Returns the default parts.
     * 
     * @return array
     */
    public function getDefaultParts()
    {
        return $this->defaultParts;
    }

    /**
     * Sets the parsed parts.
     * 
     * @param array $parts
     * @return AbstractUri
     */
    protected function setParts(array $parts)
    {
        $parts = array_intersect_key($parts, $this->getDefaultParts());
        $this->parts = array_merge($this->getDefaultParts(), $parts);

        return $this;
    }
}
