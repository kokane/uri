<?php

/**
 * This file is part of the kokane package.
 * 
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Base;

use Kokane\Uri\Parser\AbstractParser;
use Kokane\Uri\Uri;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class BaseParser extends AbstractParser
{
    /**
     * {@inheritdoc}
     */
    public function parse(Uri $uri)
    {
        $this->getParser()->parse($uri);

        $prefix = $uri->getBase()->getPrefix();
        $path = $uri->getPath();

        // Does the path use the prefix ?
        $len = strlen($prefix);
        if (strncmp($prefix, $path, $len) !== 0) {
            return;
        }

        // get the relative path
        $uri->getBase()->setPath(substr($path, $len));
    }
}
