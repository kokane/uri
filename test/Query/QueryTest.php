<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace KokaneTest\Uri\Parser;

use Kokane\Uri\Parser\ParserInterface;
use Kokane\Uri\Parser\Standard as UriParser;
use Kokane\Uri\Query\QueryHandler;
use Kokane\Uri\Query\QueryParser;
use Kokane\Uri\Uri;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class QueryTest extends TestCase
{
    /**
     * @var QueryHandler
     */
    private $query;

    /**
     * Data provider.
     * 
     * @return array
     */
    public static function getQueryDataHandler()
    {
        return [
            array(
                'http://php.net/index.php?language=PHP&version=7&year=2015',
                array(
                    'language' => 'PHP',
                    'version' => '7',
                    'year' => '2015',
                ),
                3,
            ),
            array(
                'http://php.net', [], 0
            ),
        ];
    }

    public function setUp()
    {
        $this->query = $this->getQuery('http://php.net/index.php?a=x&b=y&c=z');
    }

    /**
     * @dataProvider getQueryDataHandler
     */
    public function testValidQueryParser($uri, $expectedParams, $expectedCount)
    {
        $query = $this->getQuery($uri);

        $this->assertEquals($query->getParams(), $expectedParams);
        $this->assertCount($expectedCount, $query);
    }

    public function testSetValidParam()
    {
        $this->query->set('foo', 'bar');
        $this->assertTrue($this->query->has('foo'));
        $this->assertEquals('bar', $this->query->get('foo'));
    }

    public function testInvalidGetKeyNullReturns()
    {
        $this->assertNull($this->query->get('foo'));
    }

    public function testValidGetMethodOfQuery()
    {
        $this->assertEquals('x', $this->query->get('a'));
        $this->assertEquals('y', $this->query->get('b'));
        $this->assertEquals('z', $this->query->get('c'));
    }

    public function testNoEmptyValidCountOfParams()
    {
        $this->assertCount(3, $this->query);
    }

    public function testEmptyQueryParams()
    {
        $this->query->clear();
        $this->assertCount(0, $this->query);
    }

    public function testQueryToString()
    {
        $this->assertEquals('a=x&b=y&c=z', $this->query->getRawString());
    }

    public function testHasQueryKeys()
    {
        $keys = $this->query->getKeys();

        foreach ($keys as $key) {
            $this->assertTrue($this->query->has($key));
        }
    }

    public function testGetValuesMethod()
    {
        $this->assertEquals(['x', 'y', 'z'], $this->query->getValues());
    }

    public function testValidExtendParams()
    {
        $this->query->extend(array('foo' => 'bar'));

        $this->assertTrue($this->query->has('foo'));
    }

    /**
     * @dataProvider getQueryDataHandler
     */
    public function testRemove($uri, $expectedParams, $expectedCount)
    {
        $query = $this->getQuery($uri);
        $keys = $this->getQuery($uri)->getKeys();

        for ($len = count($keys); $len >= 1; $len--) {
            $this->assertCount($len, $query);
            $query->remove($keys[$len - 1]);
        }
    }

    public function testHasNoEmptyParams()
    {
        $this->assertFalse($this->query->isEmpty());
    }

    public function testIsEmptyParams()
    {
        $this->query->clear();
        $this->assertTrue($this->query->isEmpty());
    }

    public function testHasEmptyRawStringParseReturnNull()
    {
        $uri = $this->getUriInstance('http://php.net');
        $parser = $this->getUriParserInstance();
        $parser = $this->getQueryParserInstance($parser);

        $this->assertNull($parser->parse($uri));
    }

    public function testValidQueryToString()
    {
        $this->assertEquals('a=x&b=y&c=z', $this->query);
    }

    /**
     * Return an instance of query.
     * 
     * @param  string $uri
     * @return QueryHandler
     */
    private function getQuery($uri)
    {
        $uri = $this->getUriInstance($uri);
        $parser = $this->getUriParserInstance();
        $parser = $this->getQueryParserInstance($parser);

        $parser->parse($uri);

        return $uri->getQuery();
    }

    /**
     * Returns an instance of uri parser.
     * 
     * @return UriParser
     */
    private function getUriParserInstance()
    {
        return new UriParser();
    }

    /**
     * Returns an instance of query parser.
     * 
     * @param  ParserInterface $parser
     * @return QueryParser
     */
    private function getQueryParserInstance(ParserInterface $parser)
    {
        return new QueryParser($parser);
    }

    /**
     * Returns an instance of uri.
     * 
     * @param  Uri $uri
     * @return Uri
     */
    private function getUriInstance($uri)
    {
        return new Uri($uri);
    }
}
