<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Base;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class Base implements BaseHandler
{
    /**
     * @var string 
     */
    private $prefix;

    /**
     * @var string 
     */
    private $path;

    /**
     * Constructor sets the prefix.
     * 
     * @param string $prefix
     * @return void
     */
    public function __construct($prefix = null)
    {
        if (null !== $prefix) {
            $this->setPrefix($prefix);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function setPrefix($prefix)
    {
        $this->prefix = (string) $prefix;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPath($path)
    {
        $this->path = (string) $path;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getPath();
    }
}
