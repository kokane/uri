<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace KokaneTest\Uri\Parser;

use Kokane\Uri\Parser\Standard;
use Kokane\Uri\Uri;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class StandardTest extends TestCase
{
    /**
     * @var Standard 
     */
    private $parser;

    /**
     * @var Uri 
     */
    private $uri;

    /**
     * Creates a new Standard object for each test method.
     * 
     * @return void
     */
    public function setUp()
    {
        $uri = 'http://foo:bar@php.net:3000/path/file.php?langage=PHP#signet';

        $this->uri = new Uri($uri);
        $this->parser = new Standard();
    }

    /**
     * Data provider.
     * 
     * @return array
     */
    public static function getExpectedPartsValues()
    {
        return [
            array(
                array(
                    Standard::ATTR_SCHEME => Uri::SCHEME_HTTP,
                    Standard::ATTR_USER => 'foo',
                    Standard::ATTR_PASS => 'bar',
                    Standard::ATTR_HOST => 'php.net',
                    Standard::ATTR_PORT => 3000,
                    Standard::ATTR_PATH => '/path/file.php',
                    Standard::ATTR_QUERY => 'langage=PHP',
                    Standard::ATTR_FRAGMENT => 'signet',
                )
            ),
        ];
    }

    /**
     * @dataProvider getExpectedPartsValues
     */
    public function testBasicParse($expected)
    {
        $this->parser->parse($this->uri);
        $this->assertEquals($expected, $this->parser->getParts());
    }

    /**
     * @dataProvider getExpectedPartsValues
     */
    public function testParseUriMap($expected)
    {
        $this->parser->parse($this->uri);

        $this->assertEquals($this->uri->getScheme(), $expected[Standard::ATTR_SCHEME]);
        $this->assertEquals($this->uri->getUser(), $expected[Standard::ATTR_USER]);
        $this->assertEquals($this->uri->getPass(), $expected[Standard::ATTR_PASS]);
        $this->assertEquals($this->uri->getHost(), $expected[Standard::ATTR_HOST]);
        $this->assertEquals($this->uri->getPort(), $expected[Standard::ATTR_PORT]);
        $this->assertEquals($this->uri->getPath(), $expected[Standard::ATTR_PATH]);
        $this->assertEquals($this->uri->getFragment(), $expected[Standard::ATTR_FRAGMENT]);

        $this->assertFalse($this->uri->isSecure());
        $this->assertEquals($this->uri->getQuery()->getRawString(), $expected[Standard::ATTR_QUERY]);
    }
}
