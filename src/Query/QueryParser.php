<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Query;

use Kokane\Uri\Parser\AbstractParser;
use Kokane\Uri\Uri;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class QueryParser extends AbstractParser
{
    /**
     * {@inheritdoc}
     */
    public function parse(Uri $uri)
    {
        $this->getParser()->parse($uri);

        if ('' == $uri->getQuery()->getRawString()) {
            return;
        }

        $params = [];
        parse_str($uri->getQuery()->getRawString(), $params);

        $uri->getQuery()->setParams($params);
    }
}
