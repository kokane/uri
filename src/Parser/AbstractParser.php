<?php

/**
 * This file is part of the kokane package.
 * 
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Parser;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
abstract class AbstractParser implements ParserInterface
{
    /**
     * @var ParserInterface 
     */
    private $parser;

    /**
     * Constructor.
     * 
     * @param  ParserInterface $parser
     * @return void
     */
    public function __construct(ParserInterface $parser)
    {
        $this->setParser($parser);
    }

    /**
     * Sets the parser.
     * 
     * @param  ParserInterface $parser
     * @return AbstractParser
     */
    public function setParser(ParserInterface $parser)
    {
        $this->parser = $parser;

        return $this;
    }

    /**
     * Returns the parser.
     * 
     * @return ParserInterface
     */
    public function getParser()
    {
        return $this->parser;
    }
}
