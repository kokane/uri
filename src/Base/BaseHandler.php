<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Base;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
interface BaseHandler
{
    /**
     * Returns the prefix.
     * 
     * @return string
     */
    public function getPrefix();

    /**
     * Returns the path.
     * 
     * @return string
     */
    public function getPath();

    /**
     * Sets the prefix.
     * 
     * @param  string $prefix
     * @return BaseHandler
     */
    public function setPrefix($prefix);

    /**
     * Sets the path.
     * 
     * @param  string $path
     * @return BaseHandler
     */
    public function setPath($path);

    /**
     * Returns base value's string representation.
     * 
     * @return string
     */
    public function __toString();
}
