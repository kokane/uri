<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri\Parser;

use Kokane\Uri\Uri;

/**
 * ParserInterface is the interface all Parser must implement.
 *
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
interface ParserInterface
{
    /**
     * Parse URI.
     * 
     * @param  Uri $uri
     * @return void
     */
    public function parse(Uri $uri);
}
