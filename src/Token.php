<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kokane\Uri;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class Token
{
    const PATH_SEPARATOR = '/';
}
