<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace KokaneTest\Uri;

use Kokane\Uri\Base\Base;
use Kokane\Uri\Query\Query;
use Kokane\Uri\Uri;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * @author Vincent Letourneau <vincent@nanoninja.com>
 */
class UriTest extends TestCase
{
    /**
     * @var Uri 
     */
    private $uri;

    /**
     * Creates a new Kokane\Uri\Uri object for each test method.
     * 
     * @return void
     */
    public function setUp()
    {
        $this->uri = new Uri('http://php.net/types.php?version=7');
    }

    /**
     * @expectedException Kokane\Uri\Exception\UnexpectedTypeException
     */
    public function testUriException()
    {
        $this->uri->setUri(7);
    }

    public function TestUri()
    {
        $uri = 'http://php.net/index.html';

        $this->setUri($uri);
        $this->assertEquals($uri, $this->uri->getUri());
    }

    public function testScheme()
    {
        $this->uri->setScheme(Uri::SCHEME_HTTP);
        $this->assertEquals(Uri::SCHEME_HTTP, $this->uri->getScheme());
    }

    public function testUser()
    {
        $expected = 'foo';

        $this->uri->setUser($expected);
        $this->assertEquals($expected, $this->uri->getUser());
    }

    public function testPass()
    {
        $expected = 'bar';

        $this->uri->setPass($expected);
        $this->assertEquals($expected, $this->uri->getPass());
    }

    public function testHost()
    {
        $expected = 'www.php.net';

        $this->uri->setHost($expected);
        $this->assertEquals($expected, $this->uri->getHost());
    }

    public function testPort()
    {
        $expected = 3000;

        $this->uri->setPort($expected);
        $this->assertEquals($expected, $this->uri->getPort());
    }

    public function testBadPort()
    {
        $expected = 4000;

        $this->uri->setPort($expected);
        $this->assertNotEquals(3000, $expected);
    }

    public function testPath()
    {
        $expected = '/path/to/file.php';

        $this->uri->setPath($expected);
        $this->assertEquals($expected, $this->uri->getPath());
    }

    public function testQuery()
    {
        $this->uri->setQuery(new Query());

        $this->assertInstanceOf('\Kokane\Uri\Query\QueryHandler', $this->uri->getQuery());
    }

    public function testBase()
    {
        $this->uri->setBase(new Base());

        $this->assertInstanceOf('\Kokane\Uri\Base\BaseHandler', $this->uri->getBase());
    }

    public function testFragment()
    {
        $expected = 'signet';

        $this->uri->setFragment($expected);
        $this->assertEquals($expected, $this->uri->getFragment());
    }

    public function testIsNotSecure()
    {
        $this->assertFalse($this->uri->isSecure());
    }

    public function testIsSecure()
    {
        $this->uri->setScheme(Uri::SCHEME_HTTPS);
        $this->assertTrue($this->uri->isSecure());
    }

    public function testBadEncode()
    {
        $expected = "http%3A%2F%2Fphp.net%2Ftypes.php%3Fversion%3D7";
        
        $this->uri->decode();
        $this->assertNotEquals($expected, $this->uri);
    }

    public function testEncode()
    {
        $expected = 'http%3A%2F%2Fphp.net%2Ftypes.php%3Fversion%3D7';

        $this->uri->encode();
        $this->assertEquals($expected, $this->uri->getUri());
    }

    public function testWithEmptyToString()
    {
        $this->assertNotEquals('', $this->uri);
    }

    public function testToString()
    {
        $this->assertEquals('http://php.net/types.php?version=7', $this->uri);
    }

    /**
     * Returns an instance of uri.
     * 
     * @param  stirng $uri
     * @param  string $prefix
     * @param  string $encoded
     * @return Uri
     */
    private function getUriInstance($uri, $prefix, $encoded = true)
    {
        return new Uri($uri, $prefix, $encoded);
    }
}
