<?php

/**
 * This file is part of the Kokane package.
 *
 * (c) Vincent Letourneau <vincent@nanoninja.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Set error reporting to the level to which Kokane code must comply.
 */
error_reporting(E_ALL | E_STRICT);

/**
 * Setup autoloading
 */
define('DS', DIRECTORY_SEPARATOR);
require dirname(__DIR__).DS.'vendor'.DS.'autoload.php';
